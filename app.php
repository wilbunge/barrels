<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Demo Progressive Web Application</title>

    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=description content="This a Demo Progressive Web Application which will work in offline, has a splash screen add to home screen etc,.">
    <meta name=viewport content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="theme-color" content="#0288d1">
    <meta name="msapplication-TileColor" content="#0288d1">
    <meta name="msapplication-TileImage" content="./images/icons/mstile-150x150.png">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="apple-mobile-web-app-title" content="Progressive Web Application">
    <meta name="application-name" content="Progressive Web Application">
    <link rel="apple-touch-icon" href="assets/barrel.png">
    <link rel="icon" type="image/png" href="assets/barrel.png" sizes="32x32">
    <link rel="icon" type="image/png" href="assets/barrel.png" sizes="16x16">
    <link rel="manifest" href="./manifest.json">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,700">

    <style type="text/css">

    .ui.loader{display:none;position:absolute;top:50%;left:50%;margin:0;text-align:center;z-index:1000;-webkit-transform:translateX(-50%) translateY(-50%);transform:translateX(-50%) translateY(-50%)}.ui.loader:before{position:absolute;content:'';top:0;left:50%;width:100%;height:100%;border-radius:500rem;border:.2em solid rgba(0,0,0,.1)}.ui.loader:after{position:absolute;content:'';top:0;left:50%;width:100%;height:100%;-webkit-animation:loader .6s linear;animation:loader .6s linear;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;border-radius:500rem;border-color:#767676 transparent transparent;border-style:solid;border-width:.2em;-webkit-box-shadow:0 0 0 1px transparent;box-shadow:0 0 0 1px transparent}@-webkit-keyframes loader{from{-webkit-transform:rotate(0);transform:rotate(0)}to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes loader{from{-webkit-transform:rotate(0);transform:rotate(0)}to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}.ui.mini.loader:after,.ui.mini.loader:before{width:1rem;height:1rem;margin:0 0 0 -.5rem}.ui.tiny.loader:after,.ui.tiny.loader:before{width:1.14285714rem;height:1.14285714rem;margin:0 0 0 -.57142857rem}.ui.small.loader:after,.ui.small.loader:before{width:1.71428571rem;height:1.71428571rem;margin:0 0 0 -.85714286rem}.ui.loader:after,.ui.loader:before{width:2.28571429rem;height:2.28571429rem;margin:0 0 0 -1.14285714rem}.ui.large.loader:after,.ui.large.loader:before{width:3.42857143rem;height:3.42857143rem;margin:0 0 0 -1.71428571rem}.ui.big.loader:after,.ui.big.loader:before{width:3.71428571rem;height:3.71428571rem;margin:0 0 0 -1.85714286rem}.ui.huge.loader:after,.ui.huge.loader:before{width:4.14285714rem;height:4.14285714rem;margin:0 0 0 -2.07142857rem}.ui.massive.loader:after,.ui.massive.loader:before{width:4.57142857rem;height:4.57142857rem;margin:0 0 0 -2.28571429rem}.ui.dimmer .loader{display:block}.ui.dimmer .ui.loader{color:rgba(255,255,255,.9)}.ui.dimmer .ui.loader:before{border-color:rgba(255,255,255,.15)}.ui.dimmer .ui.loader:after{border-color:#fff transparent transparent}.ui.inverted.dimmer .ui.loader{color:rgba(0,0,0,.87)}.ui.inverted.dimmer .ui.loader:before{border-color:rgba(0,0,0,.1)}.ui.inverted.dimmer .ui.loader:after{border-color:#767676 transparent transparent}.ui.text.loader{width:auto!important;height:auto!important;text-align:center;font-style:normal}.ui.indeterminate.loader:after{animation-direction:reverse;-webkit-animation-duration:1.2s;animation-duration:1.2s}.ui.loader.active,.ui.loader.visible{display:block}.ui.loader.disabled,.ui.loader.hidden{display:none}.ui.inverted.dimmer .ui.mini.loader,.ui.mini.loader{width:1rem;height:1rem;font-size:.78571429em}.ui.inverted.dimmer .ui.tiny.loader,.ui.tiny.loader{width:1.14285714rem;height:1.14285714rem;font-size:.85714286em}.ui.inverted.dimmer .ui.small.loader,.ui.small.loader{width:1.71428571rem;height:1.71428571rem;font-size:.92857143em}.ui.inverted.dimmer .ui.loader,.ui.loader{width:2.28571429rem;height:2.28571429rem;font-size:1em}.ui.inverted.dimmer .ui.large.loader,.ui.large.loader{width:3.42857143rem;height:3.42857143rem;font-size:1.14285714em}.ui.big.loader,.ui.inverted.dimmer .ui.big.loader{width:3.71428571rem;height:3.71428571rem;font-size:1.28571429em}.ui.huge.loader,.ui.inverted.dimmer .ui.huge.loader{width:4.14285714rem;height:4.14285714rem;font-size:1.42857143em}.ui.inverted.dimmer .ui.massive.loader,.ui.massive.loader{width:4.57142857rem;height:4.57142857rem;font-size:1.71428571em}.ui.mini.text.loader{min-width:1rem;padding-top:1.78571429rem}.ui.tiny.text.loader{min-width:1.14285714rem;padding-top:1.92857143rem}.ui.small.text.loader{min-width:1.71428571rem;padding-top:2.5rem}.ui.text.loader{min-width:2.28571429rem;padding-top:3.07142857rem}.ui.large.text.loader{min-width:3.42857143rem;padding-top:4.21428571rem}.ui.big.text.loader{min-width:3.71428571rem;padding-top:4.5rem}.ui.huge.text.loader{min-width:4.14285714rem;padding-top:4.92857143rem}.ui.massive.text.loader{min-width:4.57142857rem;padding-top:5.35714286rem}.ui.inverted.loader{color:rgba(255,255,255,.9)}.ui.inverted.loader:before{border-color:rgba(255,255,255,.15)}.ui.inverted.loader:after{border-top-color:#fff}.ui.inline.loader{position:relative;vertical-align:middle;margin:0;left:0;top:0;-webkit-transform:none;transform:none}.ui.inline.loader.active,.ui.inline.loader.visible{display:inline-block}.ui.centered.inline.loader.active,.ui.centered.inline.loader.visible{display:block;margin-left:auto;margin-right:auto}

    .logo{ display: block; margin: auto; }
    	
    	.actions{
    		list-style-type: none;
    		font-family: 'Roboto';
    		color: white;
    		text-transform: uppercase;
        position: relative;

    	}

      .actions .pl{
        top:0;
        width: 100px;
        height: 100px;
        /*background: rgb(236,179,80);*/
        display: block;
        border-radius: 50%;
        /*line-height: 100px;*/
        text-align: center;
        color: black;
        text-decoration: none;
        margin: 20px 0;
        transition: all .5s;
        position: absolute;

        left: calc(50% - 50px);

      }

    	.actions.disabled{
    		opacity: .5;
    	}

    	.actions a{
    		width: 100px;
		    height: 100px;
		    background: rgb(236,179,80);
		    display: block;
		    border-radius: 50%;
		    /*line-height: 100px;*/
		    text-align: center;
		    color: black;
		    text-decoration: none;
		    margin: 20px 0;
		    transition: all .5s;
		    position: absolute;

        left: calc(50% - 50px);

    	}

    	.actions a:nth-child(1){ top:0px; }
    	.actions a:nth-child(2){ top:120px; }
    	.actions a:nth-child(3){ top:240px; }
    	.actions a:nth-child(4){ top:360px; }
      .actions a:nth-child(5){ top:480px; }

    	.actions span{ display: block; margin-top: 40px; }

    	.actions.loading a{
    		position: absolute;
    		top:10px;
    		opacity: .4;
    	}

      .actions.loading a span{ display: none;  }
      .actions .pl{ display: none; }
      .actions.loading .pl{ display: block; }

    	.actions.done a{
    		display: none;
    	}

    	.actions.done a.on{
    		display: block;
    		position: absolute;
    		top:10px;
    	}

    	.actions.done a.on span{
    		font-size:12px;
    		line-height: 30px;
    		margin-top: -40px;
    	}

    	.actions.done a.on:before{
    		content:'✓';
        display: block;
        font-size: 30px;
        margin-top: 10px;
    	}

    </style>

  </head>
  <body>

<?php 

	
	
  $data = $_GET['data'];

  $arr = explode('|', $data);

  $barrel = $arr[0];
  $lat = $arr[1];
  $lng = $arr[2];  ?>

<img class="logo" src="assets/lupulus.png">


  	<div class="actions disabled">
      <div class="ui segment pl"><div class="ui active dimmer"><div class="ui loader"></div></div></div>
  			<a href="#fill"><span>fill</span></a>
  			<a href="#load"><span>load</span></a>
  			<a href="#deliver"><span>deliver</span></a>
  			<a href="#pickup"><span>pickup</span></a>
  			<a href="#unload"><span>unload</span></a>
  		</div>

      <span class="message"></span>



  	<script type="text/javascript">

  		var lat;
  		var lng;
  		var _action;
  		var barrel;
  		var location_set = false;


  		function setLocation(x){

        <?php if($lat && $lng): ?>

          lat = <?php echo $lat; ?>;
          lng = <?php echo $lng; ?>;


        <?php else: ?>

  			lat = x.coords.latitude;
  			lng = x.coords.longitude;

      <?php endif; ?>

  			location_set = true;


  			document.querySelector('.actions').classList.remove('disabled');

  		}


	if (navigator.geolocation){
        navigator.geolocation.getCurrentPosition(setLocation);
    }


  		document.querySelector('.actions').querySelectorAll('a').forEach(
  			function(a){
  				a.addEventListener(
		  			'click',

		  			function(){

		  				if(document.querySelector('.actions').classList.contains('disabled')) return;

		  				var a = this.getAttribute('href').replace('#', '');
		  				action(a, this);

		  				return false;
		  			}
		  		);
  			}
  		);
  		

  		

  		function action(a, $a){


  			$a.classList.add('on');
  			document.querySelector('.actions').classList.add('loading');

  			var $actions = document.querySelector('.actions');

  			var xhttp = new XMLHttpRequest();
			  xhttp.onreadystatechange = function() {
			    if (this.readyState == 4 && this.status == 200) {
			    	   var jsonResponse = JSON.parse(this.responseText);

			     	
			    	   if(jsonResponse.success){

                  $a.innerHTML = a+ ' #<?php echo $barrel; ?>';
                  // document.querySelector('.actions').innerHTML = a+ ' #<?php echo $barrel; ?>';

			    	   		$actions.classList.remove('loading');
			    	   		$actions.classList.add('done');

			    	   }

			    }else{
			    console.log(this.responseText);		
			    }
			  };
			  xhttp.open("GET", "qr.php?barrel=<?php echo $barrel; ?>&action="+a+"&lat="+lat+"&lng="+lng, true);
			  xhttp.send();
  		}


  	</script>


  </body>
</html>