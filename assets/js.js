var lizt = { id:0 };

lizt.saved = false;

lizt.ui = {};
lizt.data = {};


jQuery(document).ready(

	function(){

		// lizt.ui.init();

		// lizt.data.init(
		// 	function(r){		
		// 		lizt.ui.render();
		// 		if(r.result.mode){
		// 			lizt.ui.mode(r.result.mode);
		// 		}

		// 	}
		// );

	}

);




// Controller

lizt.initMenu = function(){


	lizt.data.loadMenu(
		function(r){

			lizt.ui.renderMenu(r.result.menu);

		}
	);

}


lizt.cmd = function(a){

	var b = a.replace('$', '').split(' ');

	var cmd = b[0];
	var c = b[1];


	switch(cmd){

		case 'mode':
			lizt.mode(c);
			break;

		case 'clear':
			lizt.ui.clear();
			break;

	}



}



lizt.mode = function(m){

	switch(m){

		case 'dev':
		case 'task':
		case 'text':

			lizt.data.mode = m;
			lizt.ui.mode(m);

			lizt.data.change();

			break;
		default:
			alert('UNRECOGNIZED MODE: '+m);
			break;

	}

}




// UI

lizt.ui.init = function(){

	jQuery('header').append(jQuery('<nav>'));
	// jQuery('header').append(jQuery('<a>').addClass('menuer box-shadow-menu'));
	jQuery('header').append(jQuery('<div>').addClass('menu').append('<i class="fa fa-bars"></i><i class="fa fa-user"></i>'));
	// jQuery('header').append(jQuery('<a>').addClass('menuer box-shadow-menu').append('<i class="fa fa-bars"></i>'));
	// jQuery('header').append(jQuery('<a>').addClass('menuer box-shadow-menu').append('<i class="fa fa-user"></i>'));

	

	lizt.ui.$ = {};
	lizt.ui.$.body = jQuery(document).find('body');
	lizt.ui.$.title = jQuery(document).find('input[name=title]');
	lizt.ui.$.ol = jQuery(document).find('ol');
	lizt.ui.$.menu = jQuery('nav');
	lizt.ui.$.menuer = jQuery(document).find('.menuer');
	lizt.ui.$.shadow = jQuery('body').prepend(jQuery('<div>').addClass('shadow')).find('div.shadow');

	lizt.ui.$.title.attr('placeholder', 'Untitled');


	lizt.ui.bind(lizt.ui.$.title);	

	// focus first
	lizt.ui.$.ol.find('input').first().focus();


	lizt.initMenu();


	// Menu

	lizt.ui.$.menuer.click(
		function(){
			lizt.ui.menu();
			return false;
		}
	);
	
}

lizt.ui.clear = function(){

	lizt.ui.$.ol.find('li').each(function(i,o){
		var $li = jQuery(o);
		
		$li.removeClass('done');
		$li.find('input[name=done]').prop('checked', false);
		$li.find('textarea').val('');
	});

}


lizt.ui.mode = function(m){

	switch(m){

		case 'task':
		case 'text':
		case 'dev':
			lizt.ui.$.body.removeClass('mode dev');
			lizt.ui.$.body.removeClass('mode text');

			lizt.ui.$.body.addClass('mode '+m);



		
			

			break;

	}


}


lizt.ui.indent = function(e){

	var $li = $(e).closest('li');
	var i = $li.index();

	$(e).closest('li').addClass('child');
	lizt.data.lines[i].child = true;

}



lizt.ui.version = function(v){

	lizt.ui.$.menu.append('<version>Lizt v'+v+'</version>');

}


lizt.ui.renderMenu = function(menu){

	lizt.ui.$.menu.append(jQuery('<a>').attr('href', './').text('NUEVA LISTA'));

	for(var i in menu){
		var $a = jQuery('<a>').attr('href', './'+menu[i].id).text(menu[i].title);
		lizt.ui.$.menu.append($a);

	}
}



lizt.ui.newLine = function(){

	var $clone = lizt.ui.$.ol.find('li').first().clone();
	$clone.find('textarea[name=line]').val('');
	$clone.find('input[name=done]').prop('checked', false);
	$clone.removeClass('done');
	$clone.appendTo(lizt.ui.$.ol);
	$clone.find('textarea[name=line]').focus();


	lizt.ui.bind($clone.find('textarea[name=line]'));
	lizt.ui.bind($clone.find('input[name=done]'));

}

lizt.ui.bind = function($input){

	// console.log($input);

	if($input.attr('name')=='done'){
		$input.click(
			function(){

				if($(this).prop('checked')){
					$(this).closest('li').addClass('done');
				}else{
					$(this).closest('li').removeClass('done');
				}
			}
		);
	}

	$input.change(
		function(){
			lizt.data.change();
		}
	);


	$input.keydown(
		function(e){

			resizeTextarea(e.target);

			if((e.key == '$') && jQuery(e.target).val() == ''){
  				jQuery(e.target).closest('li').addClass('cmd');
  			}else if(jQuery(e.target).val()==''){
  				jQuery(e.target).closest('li').removeClass('cmd');
  			}

			// if(e.key=='Tab'){

			// 	lizt.ui.indent(e.target);

			// 	return false;

			// 	var i = (lizt.ui.$.ol.find('li').index(jQuery(e.target).parent()));

			// 	if(lizt.ui.$.ol.find('li').size()-1 == i){
			// 		lizt.ui.newLine();
			// 	}else{
			// 		jQuery(e.target).closest('li').next('li').find('textarea').focus();
			// 	}
			// }
		}
	);


	// key events
	$input.keypress(

		function(e){

			// console.log(jQuery(e.target).scrollHeight);
			// jQuery(e.target).style.height = "1px";
  	// 		jQuery(e.target).style.height = (25+jQuery(e.target).scrollHeight)+"px";



			
			if(e.charCode==13 || e.charCode==9){


				// command
				if(jQuery(e.target).val().indexOf('$') == 0){

					lizt.cmd(jQuery(e.target).val());

					jQuery(e.target).val('');
					jQuery(e.target).closest('li').removeClass('cmd');

					return false;
				}



				var i = (lizt.ui.$.ol.find('li').index(jQuery(e.target).parent()));

				if(lizt.ui.$.ol.find('li').size()-1 == i){
					lizt.ui.newLine();
				}else{
					jQuery(e.target).closest('li').next('li').find('textarea').focus();
				}

				

				return false;
			}else{

				lizt.data.change();

			}
		}
	);
	
}


lizt.ui.render = function(){

	lizt.ui.$.title.val(lizt.data.title);
	jQuery(document).find('title').text(lizt.data.title);


	if(!lizt.data.id){
		lizt.ui.$.title.focus();

		lizt.ui.bind(lizt.ui.$.ol.find('li textarea[name=line]'));
		lizt.ui.bind(lizt.ui.$.ol.find('li input[name=done]'));

		for (var i in [1,2,3,4,5]) {
			lizt.ui.newLine();
		};

	}else{

		lizt.ui.bind(lizt.ui.$.ol.find('li textarea[name=line]'));
		lizt.ui.bind(lizt.ui.$.ol.find('li input[name=done]'));

		for(var i in lizt.data.lines){
			lizt.ui.newLine();
		}

		for(var i in lizt.data.lines){

			if(lizt.data.lines[i].done) lizt.ui.$.ol.find('li:eq('+i+')').addClass('done');
			if(lizt.data.lines[i].child) lizt.ui.$.ol.find('li:eq('+i+')').addClass('child');

			lizt.ui.$.ol.find('li:eq('+i+')').find('textarea[name=line]').val(lizt.data.lines[i].content);
			lizt.ui.$.ol.find('li:eq('+i+')').find('input[name=done]').prop('checked', lizt.data.lines[i].done);
		}

	}


	$('body').show();

	

}


lizt.ui.menu = function(){

	if(lizt.ui.$.menu.hasClass('on')){
		lizt.ui.$.shadow.fadeOut();
		lizt.ui.$.menu.removeClass('on');
		lizt.ui.$.menu.animate({right: -lizt.ui.$.menu.outerWidth()}, function(){ lizt.ui.$.menu.hide(); });
	}else{
		lizt.ui.$.menu.addClass('on');
		lizt.ui.$.menu.show();
		lizt.ui.$.menu.animate({right: 0});
		lizt.ui.$.shadow.fadeIn();	

	}


}







// DATA

lizt.data.init = function(callback){

	lizt.data.saved = true;	
	lizt.data.lines = [];

	if(!(lizt.data.id = location.href.split('/')[location.href.split('/').length-1])){
		
		// init line objects
		for (var i in [1,2,3,4,5]) {
			lizt.data.lines.push({});
		};

		callback();

	}else{

		lizt.data.load(
			function(r){
				lizt.data.lines = r.result.lines;
				callback(r);
			}
		);

	}

	lizt.data.version(
		function(v){
			lizt.ui.version(v);
		}
	);

}

lizt.data.version = function(callback){

	jQuery.ajax({
		url: 'server/version.php',
		dataType: 'JSON',
		type: 'GET',
		success: function(r){
			if(r.success){
				lizt.data.version = r;
				callback(r.result);
				// window.history.pushState({"html":"","pageTitle":"xxx"},"", "./"+lizt.data.id);

			}else{
				alert('ERROR FETCHING VERSION');
			}
		},
		error: function(r){ console.log('ERROR FETCHING VERSION'); console.log(r); },
	});

}

lizt.data.change = function(){

	lizt.data.saved = false;
	scr = document.body.scrollTop
	location.replace('#');
	document.body.scrollTop = scr;

	clearTimeout(lizt.data.tid);

	lizt.data.tid = setTimeout(
		function(){
			console.log('Persisting...');
			lizt.data.persist();
		},
		1000
	);

}

lizt.data.load = function(callback){

	d = {id: lizt.data.id };

	jQuery.ajax({
		url: 'server/load.php',
		dataType: 'JSON',
		data: d,
		type: 'POST',
		success: function(r){
			if(r.success){
				// console.log(r);
				lizt.data.title = r.result.title;
				lizt.data.mode = r.result.mode;
				callback(r);
				// window.history.pushState({"html":"","pageTitle":"xxx"},"", "./"+lizt.data.id);

			}else{
				alert('LIST NOT FOUND');
			}
		},
		error: function(r){ console.log('ERROR LOADING'); console.log(r); },
	});

}


lizt.data.persist = function(){




	d = {id: lizt.data.id, title: lizt.ui.$.title.val(), mode: lizt.data.mode, lines: []};

	var list_done = true;


	lizt.ui.$.ol.find('li').each(function(i,o){
		var $li = $(o);
		var $done = $li.find('input[name=done]');
		var $line = $li.find('textarea[name=line]');



		if(lizt.data.lines && lizt.data.lines[i]) var child = lizt.data.lines[i].child;

		list_done = list_done && ($line.val()=='' || $done.prop('checked'))

		line_val=$line.val();

		if($line.val().indexOf('$') == 0) line_val='';

		d.lines.push(
			{
				content: line_val,
				done: $done.prop('checked'),
				child: child
			}
		);

	})

	d.done = list_done;

	jQuery.ajax({
		url: 'server/persist.php',
		dataType: 'JSON',
		data: d,
		type: 'POST',
		success: function(r){
			if(r.success){

				// Cookie Lizt if needed
				if(!lizt.data.id) lizt.data.cookie(r.result.id);

				lizt.data.id = r.result.id;
				window.history.pushState({"html":"","pageTitle":"Wolfram"},"", "./"+lizt.data.id);
				document.title = lizt.ui.$.title;
				history.pushState("", document.title, window.location.pathname);
				lizt.data.saved = true;
			}
		},
		error: function(r){ console.log('ERROR PERSISTING'); console.log(r); },
	});

}


lizt.data.cookie = function(id){

	cookie = getCookie('Lizt').split(',');

	if(cookie=='') cookie=[];

	// if(!Array.isArray(cookie)) cookie=[cookie];
	
	cookie.push(id);
	console.log('Cookie: ');
	console.log(cookie);

	setCookie('Lizt', cookie);
}



lizt.data.loadMenu = function(callback){

	d = { lizt: getCookie('Lizt').split(',') };

	jQuery.ajax({
		url: 'server/menu.php',
		dataType: 'JSON',
		data: d,
		type: 'POST',
		success: function(r){
			if(r.success){
				// console.log(r);
				lizt.data.title = r.result.title;
				callback(r);
				// window.history.pushState({"html":"","pageTitle":"xxx"},"", "./"+lizt.data.id);

			}else{
				alert('ERROR');
			}
		},
		error: function(r){ console.log('ERROR LOADING MENU'); console.log(r); },
	});

}




function setCookie(cname, cvalue, exdays) {
    var d = new Date();

    d.setTime(d.getTime() + (365*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}


function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}




var resizeTextarea = function(el) {
		    	
		    	if(el.scrollHeight>500) return;
		    	console.log(el.scrollHeight);
		        jQuery(el).css('height', 'auto').css('height', el.scrollHeight + 0);
		    };