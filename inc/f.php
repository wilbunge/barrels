<?php 


	ini_set('display_errors', 0);
	ini_set('display_startup_errors', 0);
	error_reporting(0);



	function get_transactions(){

		include_once('inc/db.php');

		return db::read('transactions', '*', 'TRUE', 'ORDER BY id DESC');

	}
	
	function get_link($barrel, $lat='', $lng=''){

		$link = "app.php?data=$barrel|$lat|$lng";

		return $link;

	}

	
	function qr($barrel, $status, $lat, $lng){

		include_once('inc/db.php');

		date_default_timezone_set("AMerica/Montevideo");

		db::create('transactions', 'barrel,status,lat,lng,date', "$barrel,'$status','$lat','$lng','".date('Y-m-d h:i:s')."'");

	}

	function add_new_barrel(){

		include_once('inc/db.php');

		db::create('barrels', '');

		return mysqli_insert_id();

	}


 ?>