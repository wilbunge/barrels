<?php 

	include('inc/f.php');

	date_default_timezone_set("America/Montevideo");

	if(isset($_GET['ajax'])){

		$time = $_GET['time'];
		$debug = isset($_GET['debug']);

		$transactions = get_transactions();

		$buffer = [];
		$data = [];

		$data['transactions'] = [];

		while($transaction = mysql_fetch_array($transactions)){

			if(in_array($transaction['barrel'], $buffer)) continue;

			$id = $transaction['id'];
			$barrel = $transaction['barrel'];
			$lat = $transaction['lat'];
			$lng = $transaction['lng'];
			$date = $transaction['date'];
			$action = $transaction['status'];


			if($id <= $time) continue;

			if($time < $id) $time = $id;

			$data['transactions'][] = $transaction;

			$date = date('d/m h:i:s A', strtotime($date));

			$buffer[] = $barrel;

		}

		$data['time'] = $time;

		$json = json_encode($data);

		die($json);
	}


 ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Barrels™</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

  </head>
  <body>


    <div id="map"></div>
    <script>


    	var markers = [];


      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.791385, lng: -55.93241},
          zoom: 12
        });


        var barrels = [];

        barrels.push({lat: -34.751380, lng: -55.91241});
        barrels.push({lat: -34.720320, lng: -55.93211});
        barrels.push({lat: -34.7913803, lng: -55.95201});


        var icon = {
		    url: "assets/lupulus.png", // url
		    scaledSize: new google.maps.Size(64, 44), // scaled size
		    origin: new google.maps.Point(0,0), // origin
		    anchor: new google.maps.Point(0, 0) // anchor
		};

		var icon_truck = {
		    url: "assets/truck.png", // url
		    scaledSize: new google.maps.Size(72, 56), // scaled size
		    origin: new google.maps.Point(0,0), // origin
		    anchor: new google.maps.Point(0, 0) // anchor
		};


        var marker = new google.maps.Marker({
		    position: {lat: -34.791385, lng: -55.93241},
		    map: map,
		    icon: icon
		});

		var marker = new google.maps.Marker({
		    position: {lat: -34.782385, lng: -55.90241},
		    map: map,
		    icon: icon_truck
		});

<?php 

	$transactions = get_transactions();

	$buffer = [];

	while($transaction = mysql_fetch_array($transactions)){



		if(in_array($transaction['barrel'], $buffer)) continue;

		$id = $transaction['id'];
		$barrel = $transaction['barrel'];
		$lat = $transaction['lat'];
		$lng = $transaction['lng'];
		$date = $transaction['date'];
		$action = $transaction['status'];

		$date = date('d/m h:i:s A', strtotime($date)+3600*4);

		$buffer[] = $barrel;

		// continue;

	 ?>

	 // console.log('<?php echo $barrel; ?>');

	 var icon = {
			    url: "assets/barrel.png", // url
			    scaledSize: new google.maps.Size(40, 40), // scaled size
			    origin: new google.maps.Point(0,0), // origin
			    anchor: new google.maps.Point(0, 0) // anchor
			};



	  		var contentString = `

	  			<h2><?php echo $barrel; ?> # IPA Session</h2>

	  			<ul>

	  				<li><?php echo $date; ?>
	  				<li><?php echo $action; ?>
	  			</ul>

	  		`;

	  		var infowindow = new google.maps.InfoWindow({
	    		content: contentString
	  		});


			var marker = new google.maps.Marker({
		    	position: {lat: <?php echo $lat; ?>, lng: <?php echo $lng; ?>},
		    	map: map,
		    	icon: icon
			});

			marker.infowindow = infowindow;


			marker.addListener('click', function() {
				this.infowindow.open(map, this);
			});

			markers[<?php echo $barrel; ?>] = marker;


<?php } ?>


		// for(var i in barrels){

		// 	var icon = {
		// 	    url: "assets/barrel.png", // url
		// 	    scaledSize: new google.maps.Size(40, 40), // scaled size
		// 	    origin: new google.maps.Point(0,0), // origin
		// 	    anchor: new google.maps.Point(0, 0) // anchor
		// 	};



	 //  		var contentString = `

	 //  			<h2>35 # IPA Session</h2>

	 //  			<ul>
	 //  				<li>Carrasco Beer House
	 //  				<li>Hace 32 días!
	 //  			</ul>

	 //  		`;

	 //  		var infowindow = new google.maps.InfoWindow({
	 //    		content: contentString
	 //  		});


		// 	var marker = new google.maps.Marker({
		//     	position: barrels[i],
		//     	map: map,
		//     	icon: icon
		// 	});


		// 	marker.addListener('click', function() {
		// 		infowindow.open(map, this);
		// 	});

		// }


		survey();

      }

      var time = '<?php echo $id; ?>';




      function setMarker(transaction){

      	if(markers[transaction.barrel]) markers[transaction.barrel].setMap(null);

      	var icon = {
			    url: "assets/barrel.png", // url
			    scaledSize: new google.maps.Size(40, 40), // scaled size
			    origin: new google.maps.Point(0,0), // origin
			    anchor: new google.maps.Point(0, 0) // anchor
			};



	  		var contentString = `

	  			<h2>${transaction.barrel} # IPA Session</h2>

	  			<ul>
	  				<li>Carrasco Beer House
	  				<li>${transaction.date}
	  				<li>${transaction.barrel}
	  				<li>${transaction.status}
	  				<li>transaction id: ${transaction.id}
	  			</ul>

	  		`;

	  		var infowindow = new google.maps.InfoWindow({
	    		content: contentString
	  		});


			var marker = new google.maps.Marker({
		    	position: {lat: parseFloat(transaction.lat), lng: parseFloat(transaction.lng)},
		    	map: map,
		    	icon: icon
			});

			marker.infowindow = infowindow;


			marker.addListener('click', function() {
				this.infowindow.open(map, this);
				resetAnimations();
			});


			resetAnimations();
			marker.setAnimation(google.maps.Animation.BOUNCE);

			markers[transaction.barrel] = marker;

      }


      function resetAnimations(){

      	markers.forEach(
      		function(marker){
      			marker.setAnimation(null);
      		}
      	);

      }

      function survey(){

      	// console.log(`?time=${time}&ajax`);

      	jQuery.getJSON(
      		`?time=${time}&ajax`,
      		function(r){
      			
      			time = r.time;

      			// console.log(r.time);

      			if(r.transactions){

      				// console.log(r);

      				r.transactions.forEach(
      					function(transaction){
      						setMarker(transaction);
      					}
      				);


      			}


      			setTimeout(
					function(){
						survey();
					},

					5000
				);

      			
      		}
      		);

      }


      



    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD09zQ9PNDNNy9TadMuzRV_UsPUoWKntt8&callback=initMap"
    async defer></script>
  </body>
</html>